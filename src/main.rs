#![allow(dead_code)]
#![allow(unused_variables)]

/*
https://hack64.net/Thread-SM64-EEPROM
https://hack64.net/wiki/doku.php?id=super_mario_64:eeprom_map
http://jul.rustedlogic.net/thread.php?id=6331
https://www.smwcentral.net/?p=viewthread&t=74721
http://micro-64.com/database/gamesave.shtml
http://bryc.github.io/sm64eep/#pg=0
http://shygoo.net/o64-backup/sm64_hacking/EEPROM%20-%20Origami64.html

https://github.com/DarkMatterCore/sm64dcsc
https://github.com/sonich2401/SM64_Save_File_Converter
https://github.com/bryc/sm64eep
https://github.com/Render96/Render96ex/blob/master/SAVE_FORMAT.MD

0x0000 - Save Data 1
0x0038 - Save Data 1 (backup)
0x0070 - Save Data 2
0x00A8 - Save Data 2 (backup)
0x00E0 - Save Data 3
0x0118 - Save Data 3 (backup)
0x0150 - Save Data 4
0x0188 - Save Data 4 (backup)
0x01C0 - Main Menu Data
0x01E0 - Main Menu Data (backup)
0x01FF - End

coin score age stuff
https://github.com/sm64pc/sm64ex/blob/8152239732219467593fdbd139e615b9d8a0d41e/src/game/save_file.c
https://github.com/sm64pc/sm64ex/blob/8152239732219467593fdbd139e615b9d8a0d41e/src/game/text_save.inc.h
https://github.com/sm64pc/sm64ex/blob/8152239732219467593fdbd139e615b9d8a0d41e/src/game/save_file.h

look for coinScoreAges in the above files

array of 4 u32 integers is used for data, one per file

https://github.com/sm64pc/sm64ex/blob/8152239732219467593fdbd139e615b9d8a0d41e/include/macros.h

BSWAP32 macro is used before saving

    ( (((x) >> 24) & 0x000000FF) | (((x) >>  8) & 0x0000FF00) | \
      (((x) <<  8) & 0x00FF0000) | (((x) << 24) & 0xFF000000) )

...which appears to simply change the endian like this:
12345678 -> 78563412
*/

use binread::{io::Cursor, BinRead, BinReaderExt};
use bit_vec::BitVec;
use std::collections::HashMap;
use std::fs;
use std::path::Path;

// 512 bytes
#[derive(BinRead)]
struct EepRomRaw {
    // 448 bytes
    #[br(count = 4)]
    saves: Vec<SaveFileRaw>,
    // 64 bytes
    menu: MenuFileRaw,
}

// 64 bytes
#[derive(BinRead)]
struct MenuFileRaw {
    #[br(count = 2)]
    copies: Vec<MenuDataRaw>,
}

// 32 bytes
#[derive(BinRead)]
struct MenuDataRaw {
    // 16 bytes
    #[br(count = 4)]
    coin_score_ages: Vec<u32>,
    // 2 bytes
    sound_mode: u16,
    // 2 bytes
    language: u16,
    // 8 bytes
    #[br(count = 2)]
    unused: Vec<u32>,
    // 2 bytes, always 0x4849
    magic: u16,
    // 2 bytes
    checksum: u16,
}

#[derive(BinRead)]
struct SaveFileRaw {
    #[br(count = 2)]
    copies: Vec<SaveDataRaw>,
}

// 56 bytes
#[derive(BinRead)]
struct SaveDataRaw {
    // 8 bytes
    cap: CapDataRaw,
    // 4 bytes, includes castle stars as first byte
    flags: u32,
    // 15 bytes
    #[br(count = 15)]
    stars: Vec<u8>,
    // 10 bytes
    #[br(count = 10)]
    secret_stars: Vec<u8>,
    // 15 bytes
    #[br(count = 15)]
    coins: Vec<u8>,
    // 2 bytes, always 0x4441
    magic: u16,
    // 2 bytes
    checksum: u16,
}

// 8 bytes
#[derive(BinRead)]
struct CapDataRaw {
    // 0x08 = klepto (shifting sand land)
    // 0x0A = mrblizzard (snowman's land)
    // 0x24 = ukiki (tall tall mountain)
    course_id: u8,
    area_id: u8, // Always 0x01
    #[br(count = 3)]
    coord: Vec<u16>,
}

struct CapData {
    loss_type: String,
    level: String,
    area: u8, // Always 0x01, but 0x02 is technically possible
}

struct CourseData {
    coins: u8,
    stars: BitVec,
    cannon: bool,
}

struct MenuData {
    coin_score_age: Vec<u32>,
    sound_mode: String,
}

fn main() {
    let save_path: &std::path::Path = Path::new("save.bin");

    if save_path.exists() {
        let save_size: std::fs::Metadata = std::fs::metadata(save_path).unwrap();
        if save_size.len() == 512 {
            let raw_eep = raw_from_bytes(fs::read(save_path).unwrap(), true);
            pretty_eep(&raw_eep);
            //show_raw_eep(&raw_eep);
        } else {
            panic!("File must be 512 bytes!");
        }
    } else {
        panic!("File not found!");
    }
}

fn raw_from_bytes(save_bytes: Vec<u8>, little_endian: bool) -> EepRomRaw {
    let mut reader: std::io::Cursor<&[u8]> = Cursor::new(save_bytes.as_slice());

    // Decide how to interpret save data
    let raw_eep: EepRomRaw = if little_endian {
        reader.read_le().unwrap()
    } else {
        reader.read_be().unwrap()
    };

    return raw_eep;
}

fn get_menu(menu: &MenuDataRaw) -> MenuData {
    let sound_map: HashMap<u16,&str> = HashMap::from([
        (0x00,"stereo"),
        (0x01,"mono"),
        (0x02,"headset"),
    ]);

    let results: MenuData = MenuData {
        coin_score_age: menu.coin_score_ages.clone(),
        sound_mode: sound_map[&menu.sound_mode].to_string(),
    };

    return results;
}

fn get_bonus(save: &SaveDataRaw) -> HashMap<String, CourseData> {
    let flagbytes: [u8; 4] = u32::to_be_bytes(save.flags);
    let mut hub_bits: BitVec = BitVec::from_bytes(&flagbytes);
    hub_bits.split_off(8);

    let secret_bytes: &[u8] = &save.secret_stars;

    let star_list: [(&str, &mut BitVec); 11] = [
        ("hub", &mut hub_bits),
        ("bitdw", &mut BitVec::from_bytes(&[secret_bytes[0]])),
        ("bitfs", &mut BitVec::from_bytes(&[secret_bytes[1]])),
        ("bits", &mut BitVec::from_bytes(&[secret_bytes[2]])),
        ("pss", &mut BitVec::from_bytes(&[secret_bytes[3]])),
        ("cotmc", &mut BitVec::from_bytes(&[secret_bytes[4]])),
        ("totwc", &mut BitVec::from_bytes(&[secret_bytes[5]])),
        ("vcutm", &mut BitVec::from_bytes(&[secret_bytes[6]])),
        ("wmotr", &mut BitVec::from_bytes(&[secret_bytes[7]])),
        ("sa", &mut BitVec::from_bytes(&[secret_bytes[8]])),
        ("unu", &mut BitVec::from_bytes(&[secret_bytes[9]])),
    ];

    let mut results: HashMap<String, CourseData> = HashMap::new();

    for i in star_list {
        let temp: CourseData = CourseData {
            coins: 0,
            stars: if i.0 == "hub" {
                i.1.split_off(3)
            } else if i.0 == "pss" {
                i.1.split_off(6)
            } else {
                i.1.split_off(7)
            },
            cannon: false,
        };
        results.insert(i.0.to_string(), temp);
    }

    return results;
}

fn get_courses(save: &SaveDataRaw) -> HashMap<String, CourseData> {
    let short_names: [&str; 15] = [
        "bob", "wf", "jrb", "ccm", "bbh", "hmc", "lll", "ssl", "ddd", "sl", "wdw", "ttm", "thi",
        "ttc", "rr",
    ];

    let mut result: HashMap<String, CourseData> = HashMap::new();

    for i in 0..15 {
        let mut temp: BitVec = BitVec::from_bytes(&[save.stars[i]]);
        result.insert(
            short_names[i].to_string(),
            CourseData {
                coins: save.coins[i],
                stars: temp.split_off(1),
                cannon: temp.pop().unwrap(),
            },
        );
    }

    return result;
}

fn get_cap(save: &SaveDataRaw) -> CapData {
    let thief_map: HashMap<u8, &str> =
        HashMap::from([(0x08, "klepto"), (0x0A, "mrblizzard"), (0x24, "ukiki")]);

    let level_map: HashMap<u8, &str> = HashMap::from([(0x08, "ssl"), (0x0A, "sl"), (0x24, "ttl")]);

    let cap = CapData {
        loss_type: thief_map[&save.cap.course_id].to_string(),
        level: level_map[&save.cap.course_id].to_string(),
        area: save.cap.area_id,
    };

    return cap;
}

fn pretty_eep(raw_eep: &EepRomRaw) {
    let save_num: usize = 0;

    let save = &raw_eep.saves[save_num].copies[0];
    let menudat = &raw_eep.menu.copies[0];

    let cap = get_cap(save);
    let courses = get_courses(save);
    let bonus = get_bonus(save);
    let menu = get_menu(menudat);

    println!("[cap]");
    println!("type: {}", cap.loss_type);
    println!("level: {}", cap.level);
    println!("area: {}", cap.area);

    println!("[courses]");
    for (key, value) in courses.into_iter() {
        println!(
            "{:}: \"{:?}, {:?}, {:?}\"",
            key, value.coins, value.stars, value.cannon as u8
        );
    }

    println!("[bonus]");
    for (key, value) in bonus.into_iter() {
        println!("{:}: {:?}", key, value.stars);
    }

    println!("[menu]");
    println!("coin_score_age: {:08X?}", menu.coin_score_age);
    println!("sound_mode: {:}", menu.sound_mode);
}

fn show_raw_eep(raw_eep: &EepRomRaw) {
    for i in 0..raw_eep.saves.len() {
        for j in 0..raw_eep.saves[i].copies.len() {
            if j % 2 == 0 {
                println!("Save {:#} (Raw Data)", i);
            } else {
                println!("Save {:#} Backup (Raw Data)", i);
            }

            let temp = &raw_eep.saves[i].copies[j];

            println!("    Cap");
            println!("        Course ID {:#04X}", temp.cap.course_id);
            println!("        Area ID {:#04X}", temp.cap.area_id);
            println!("        Coordinates 0x{:04X?}", temp.cap.coord);
            println!("    Castle Stars & Flags {:#010X}", temp.flags);
            println!("    Stars 0x{:X?}", temp.stars);
            println!("    Secret Stars 0x{:X?}", temp.secret_stars);
            println!("    Coins 0x{:X?}", temp.coins);
            println!("    Magic {:#06X}", temp.magic);
            println!("    Checksum {:#06X}", temp.checksum);
        }
    }

    for i in 0..1 {
        if i % 2 == 0 {
            println!("Menu (Raw Data)");
        } else {
            println!("Menu Backup (Raw Data)");
        }

        let temp = &raw_eep.menu.copies[i];

        println!("    Coin Score Ages {:X?}", temp.coin_score_ages);
        println!("    Sound {:#06X}", temp.sound_mode);
        println!("    Language {:#06X}", temp.language);
        println!("    Unused 0x{:08X?}", temp.unused);
        println!("    Magic {:#06X}", temp.magic);
        println!("    Checksum {:#06X}", temp.checksum);
    }
}
